import os
import datetime
from collections import defaultdict

import pytz
import pymysql.cursors
from http.client import IncompleteRead # Python 3
from time import sleep

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream

from requests.packages.urllib3.exceptions import ProtocolError, ReadTimeoutError


def get_already_seen():
    connection = get_connection()
    t_set = set()
    try:
        with connection.cursor() as cursor:
            sql = 'SELECT DISTINCT tweet_id FROM display_tweet'
            cursor.execute(sql)
            result = cursor.fetchall()
            for d in result:
                t_set.add(d['tweet_id'])
    finally:
        connection.close()
    return t_set


counter = 0
likes_counter = 0
db_counter = 0
NUM_OF_LIKES = 1000
tweet_set = set()
funny_words = ['rofl', 'lmao', 'lmfao',
               'hahaha', 'hahahaha', 'hahahahaha', 'hahahahaha', 'hahahahahaha', 'hahahahahahaha',
               'win the internet', 'won the internet']

access_token = os.environ.get('ACCESS_TOKEN')
access_token_secret = os.environ.get('ACCESS_SECRET')
consumer_key = os.environ.get('CONSUMER_KEY')
consumer_secret = os.environ.get('CONSUMER_SECRET')


def get_connection():
    connection = pymysql.connect(host='localhost',
                                 user=os.environ.get("LOCAL_DB_USER", ''),
                                 password=os.environ.get("LOCAL_DB_PASSWORD", ''),
                                 db=os.environ.get("LOCAL_DB_NAME", ''),
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    return connection


def status_to_db(id_str, date, screen_name, lang, text):
    global db_counter
    connection = get_connection()
    mydate = datetime.datetime.now(tz=pytz.utc)
    try:
        with connection.cursor() as cursor:
            sql = "INSERT INTO display_tweet (tweet_id, tweet_date, screen_name, my_date, funny, published, candidate, lang, text) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
            cursor.execute(sql, (id_str, date, screen_name, mydate, 0, 0, 0, lang, text))
        connection.commit()
    finally:
        db_counter += 1
        connection.close()


def message_to_db(message_type, message_status):
    connection = get_connection()
    message_date = datetime.datetime.now(tz=pytz.utc)
    try:
        with connection.cursor() as cursor:
            sql = "INSERT INTO display_message (message_type, message_status, message_date) VALUES (%s, %s, %s)"
            cursor.execute(sql, (message_type, message_status, message_date))
        connection.commit()
    finally:
        connection.close()    


def has_photo(status):
    entities = status.entities if hasattr(status, 'entities') else None  # type(entities) = dict
    media_list = entities.get('media', None) if entities is not None else None
    media = media_list[0] if media_list is not None else None
    if media is not None and media['type'] == 'photo':
        if media['expanded_url'].split('/')[6] == 'photo':
            return True
    return False


def process_status(status):
    
    likes = status.favorite_count
    if likes > NUM_OF_LIKES:
        global likes_counter
        likes_counter += 1
        if has_photo(status):
            id_str = status.id_str
            date = pytz.utc.localize(status.created_at)  # status.created_at = naive datetime
            screen_name = status.user.screen_name
            lang = status.lang
            text = status.text
            if id_str not in tweet_set:
                tweet_set.add(id_str)
                # print('STATUS')
                # print(id_str)
                # print(date)
                # print(screen_name)
                # print(status.entities['media'][0]['type'])  #type(status.entities['media']) = list, [0] = dict
                # print(status.entities['media'][0])
                status_to_db(id_str, date, screen_name, lang, text)
                # print('--------------------------')


def worker_function(status):
    # retweets 
    if hasattr(status, 'retweeted_status'):
        org_status = status.retweeted_status
        # check if quoted_status 
        if hasattr(org_status, 'quoted_status'):
            quoted_status = org_status.quoted_status
            process_status(quoted_status)
        # normal retweet
        else:
            process_status(org_status)
            
    elif hasattr(status, 'quoted_status'):
        quoted_status = status.quoted_status
        process_status(quoted_status)
    else:
        return          


class StdOutListener(StreamListener):

    def on_status(self, status):
        try:
            worker_function(status)
        except Exception as e:
            print(e)

    def on_error(self, status_code):
        message_to_db('error', 1)
        print('Error status code: {}'.format(str(status_code)))
        if status_code == 420:
            # returning False in on_data disconnects the stream
            return False
        return True

    def on_limit(self, track):
        # how many undelivered tweets
        # message_to_db('limit', track)
        return True

    def on_exception(self, exception):
        message_to_db('exception', 1)
        print('PRINT iz on_exception')
        print(exception)
        return True

    def on_disconnect(self, notice):
        message_to_db('disconnect', 1)
        print(type(notice))
        if type(notice) != str:
            notice = str(notice)
        print('Disconnect notice: {}'.format(notice))

    def on_warning(self, notice):
        message_to_db('warning', 1)
        print(type(notice))
        if type(notice) != str:
            notice = str(notice)
        print('Warning notice: {}'.format(notice))

    def on_timeout(self):
        print('Time out zZzZz')
        message_to_db('time_out', 1)
        return True 

    def keep_alive(self):
        message_to_db('Keep_alive', 1)
        return True


if __name__ == '__main__': 
    # This handles Twitter authetification and the connection to Twitter Streaming API
    while True:
        try:
            tweet_set = get_already_seen()
            l = StdOutListener()
            auth = OAuthHandler(consumer_key, consumer_secret)
            auth.set_access_token(access_token, access_token_secret)
            stream = Stream(auth, l)
            print('--------------------------------------------------')
            print('START STREAM: ', datetime.datetime.now(tz=pytz.utc))
            stream.filter(track=funny_words, stall_warnings=True)


        except KeyboardInterrupt:
            message_to_db('kb_end', 1)
            print()
            print('STOP STREAM: ', datetime.datetime.now(tz=pytz.utc))
            print('Number of all tweets: {}'.format(str(counter)))
            print('Likes: {}'.format(likes_counter))
            print('DB: {}'.format(db_counter))
            print('Set length: {}'.format(str(len(tweet_set))))
            # stream.disconnect()
            break

        except ProtocolError as pe:
            print('PROTOCOL ERROR TIME', datetime.datetime.now(tz=pytz.utc))
            print(pe)

        except ReadTimeoutError as rte:
            print('READ TIMEOUT ERROR TIME', datetime.datetime.now(tz=pytz.utc))
            print(rte)

        except Exception as e:
            print('EXCEPTION: ', datetime.datetime.now(tz=pytz.utc))
            print(e)